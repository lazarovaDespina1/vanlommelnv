//
//  FullOffer.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/10/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import Foundation

class Offer {
    var offerName: String!
    var offerImage: String!
    var vehicles: [Vehicle]!
    
    init(offer: [String: Any]) {
        offerName = offer["offerName"] as! String
        offerImage = offer["offerImageName"] as! String
        vehicles = [Vehicle]()
        for vehicle in offer["vechicles"] as! [[String:Any]] {
            vehicles.append(Vehicle(vehicle: vehicle))
        }
    }
}
