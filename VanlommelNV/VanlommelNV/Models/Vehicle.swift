//
//  Vechicle.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/10/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import Foundation

class Vehicle {
    var name: String!
    var images: [String]!
    var details: [VehicleDetails]!
    
    init(vehicle: [String: Any]) {
        name = vehicle["Name"] as! String
        images = vehicle["imageUrls"] as! [String]
        details = [VehicleDetails]()
        
        for detail in vehicle["details"] as! [[String:Any]] {
            details.append(VehicleDetails(details: detail))
            
        }
    }
}
