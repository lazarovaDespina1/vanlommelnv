//
//  VichicleDetails.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/10/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//
import Foundation

class VehicleDetails {
    var detailTitle: String!
    var properties: [String]!
    
    init(details: [String: Any]) {
        detailTitle = details["detailsTitle"] as! String
        properties = details["properties"] as! [String]
    }
}
