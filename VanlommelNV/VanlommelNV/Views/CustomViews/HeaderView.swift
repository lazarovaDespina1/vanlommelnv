//
//  HeaderView.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/13/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit
import Foundation
import UIKit
protocol HeaderViewDelegate {
    func collectionViewDidScroll(contentX : CGFloat)
    func didSelectCollectionViewCell(indexRow : Int)
}

class HeaderView : UIView {
    
    var hotelImageView : UIImageView!
    var collectionView : UICollectionView!
    var array: [String] = []
    var backgroundImages: [String] = []
    var delegate : HeaderViewDelegate!
    var pageWhereLineisShown = 0
    var backImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        self.backgroundColor = UIColor.white
        hotelImageView = UIImageView()
        hotelImageView.contentMode = .scaleAspectFit
        hotelImageView.image = #imageLiteral(resourceName: "logo")
        
        backImageView = UIImageView()
        backImageView.contentMode = .scaleToFill
        backImageView.alpha = 0.5
        
        collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: ItemsFlowLayout())
        collectionView.register(TextCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.setContentOffset(CGPoint(x: self.frame.size.width, y: 0), animated: true)
        collectionView.delegate = self
        collectionView.bounces = false
        collectionView.contentInset = UIEdgeInsets(top: 0, left: UIScreen.main.bounds.width/2-UIScreen.main.bounds.width/6, bottom: 0, right: 0)
        
        self.addSubview(backImageView)
        self.addSubview(hotelImageView)
        self.addSubview(collectionView)
    }
    
    func setupConstraints() {
        hotelImageView.snp.makeConstraints { make in
            make.center.equalTo(self)
            make.width.height.equalTo(48)
        }
        collectionView.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(self)
            make.height.equalTo(self).dividedBy(4)
        }
        backImageView.snp.makeConstraints { make in
            make.left.right.bottom.top.equalTo(self)
        }
    }
}

extension HeaderView : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TextCell
        cell.label.text = array[indexPath.row].uppercased()
        if pageWhereLineisShown == indexPath.row {
            cell.line.isHidden = false
            cell.label.textColor = UIColor.red
            backImageView.kf.setImage(with: URL(string: backgroundImages[indexPath.row]))
        } else {
            cell.label.textColor = UIColor.black
        }
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate.collectionViewDidScroll(contentX: scrollView.contentOffset.x)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! TextCell
        pageWhereLineisShown = indexPath.row
        cell.label.textColor = UIColor.red
        delegate.didSelectCollectionViewCell(indexRow: indexPath.row)
    }
}

