//
//  NavigationView.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/10/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit


class NavigationView: UIView {
    var leftButton : UIButton!
    var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.backgroundColor = .black
        
        titleLabel = UILabel()
        titleLabel.text = "Vanlommel nv"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 22.0)
        titleLabel.textColor = UIColor.red
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5
        titleLabel.numberOfLines = 0
        titleLabel.backgroundColor = .white

        leftButton = UIButton()
        
        self.addSubview(titleLabel)
        self.addSubview(leftButton)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints{ make in
            make.top.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-10)
            make.left.equalTo(leftButton.snp.right).offset(20)
            make.right.equalTo(self).offset(-20)
        }
        
        leftButton.snp.makeConstraints { make in
            make.left.equalTo(self).offset(15)
            make.top.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-10)
            make.width.equalTo(self.snp.width).dividedBy(8)
        }
    }
}
