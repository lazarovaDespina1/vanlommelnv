//
//  VechiclesCollectionViewCell.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/10/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit
import Kingfisher

class VehiclesCollectionViewCell: UICollectionViewCell {
    var nameLabel: UILabel!
    var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        contentView.backgroundColor = .white
        
        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        
        nameLabel = UILabel()
        nameLabel.textAlignment = .right
        nameLabel.textColor = UIColor.black

        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.minimumScaleFactor = 0.5
        
        contentView.addSubview(imageView)
        contentView.addSubview(nameLabel)
    }
    
    func setupConstraints() {
       nameLabel.snp.makeConstraints{ make in
            make.left.equalTo(self).offset(5)
            make.bottom.equalTo(self).offset(-10)
            make.right.equalTo(self).offset(-10)
            make.height.equalTo(20)
        }
        
        imageView.snp.makeConstraints{ make in
            make.left.top.equalTo(self.contentView).offset(10)
            make.right.equalTo(self.contentView).offset(-10)
            make.right.bottom.equalTo(self.contentView).offset(-20)
        }
    }
    
    func setupCell(vechicleName: String, image: String) {
        nameLabel.text = vechicleName.uppercased()
        imageView.kf.setImage(with: URL(string: image))
    }
}
