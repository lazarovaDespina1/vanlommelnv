//
//  VehiclesTableViewCell.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/13/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class VehiclesTableViewCell: UITableViewCell {
    var nameLabel: UILabel!
    var vehicleImageView: UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        vehicleImageView.image = UIImage()
        nameLabel.text = ""
    }
    func setupViews() {
        contentView.backgroundColor = .white
        
        self.selectionStyle = .none
        
        vehicleImageView = UIImageView()
        vehicleImageView.contentMode = .scaleAspectFit
        
        nameLabel = UILabel()
        nameLabel.textAlignment = .left
        nameLabel.textColor = UIColor.black
        nameLabel.adjustsFontSizeToFitWidth = true
        
        contentView.addSubview(vehicleImageView)
        contentView.addSubview(nameLabel)
    }
    
    func setupConstraints() {
        nameLabel.snp.makeConstraints{ make in
            make.left.equalTo(self).offset(25)
            make.bottom.equalTo(self).offset(-10)
            make.right.equalTo(self).offset(-10)
            make.height.equalTo(20)
        }
        
        vehicleImageView.snp.makeConstraints{ make in
            make.left.top.equalTo(self.contentView).offset(10)
            make.right.bottom.equalTo(self.contentView).offset(-10)
            make.bottom.equalTo(self.contentView).offset(-40)
        }
    }
    
    func setupCell(vechicleName: String, image: String) {
        nameLabel.text = vechicleName.uppercased()
        vehicleImageView.kf.setImage(with: URL(string: image)!)
    }
}
