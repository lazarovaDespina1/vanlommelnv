//
//  OfferCollectionViewCell.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/13/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit
protocol OfferCellDelegate{
    func presentVC(vehicle: Vehicle)
}


class OfferCollectionViewCell: UICollectionViewCell {
    var tableView : UITableView!
    var vehicles: [Vehicle]!
    
    var offerDelegate: OfferCellDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //vehicles = [Vehicle]()
        //tableView.reloadData()
    }
    func setupViews() {
        vehicles = [Vehicle]()
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(VehiclesTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        self.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(self)
            make.top.equalTo(self).offset(10)
        }
    }
    
}

extension OfferCollectionViewCell : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.size.height/3
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VehiclesTableViewCell
       let vehicle = vehicles[indexPath.row]
        cell.setupCell(vechicleName: vehicle.name, image: vehicle.images[0])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        offerDelegate?.presentVC(vehicle: vehicles[indexPath.row])
    }
    
}

