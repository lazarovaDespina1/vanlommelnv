//
//  TextCell.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/13/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class TextCell: UICollectionViewCell {
    
    var label : UILabel!
    var line : UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        self.backgroundColor = UIColor.clear
        label = UILabel()
        label.textColor = UIColor.red
        label.textAlignment = NSTextAlignment.center
        
        
        line = UIView()
        line.backgroundColor = UIColor.clear
        line.isHidden = true
        
        self.addSubview(label)
        self.addSubview(line)
    }
    
    func setupConstraints() {
        label.snp.makeConstraints { make in
            make.edges.equalTo(self.contentView)
        }
        
        line.snp.makeConstraints { make in
            make.left.right.equalTo(self.contentView)
            make.bottom.equalTo(self.contentView).offset(-3)
            make.height.equalTo(2)
        }
    }
    
}
