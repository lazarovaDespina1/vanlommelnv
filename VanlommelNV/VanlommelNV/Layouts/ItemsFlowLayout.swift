//
//  ItemsFlowLayout.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/13/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class ItemsFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout() {
        
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
        scrollDirection = .horizontal
    }
    
    override var itemSize: CGSize {
        set{}
        get{
            let x = ((self.collectionView?.frame.width)!)/3
            let size = CGSize(width: x, height: x)
            print(size)
            return size
        }
        
    }
}
