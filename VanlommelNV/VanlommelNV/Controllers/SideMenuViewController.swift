//
//  SideMenuViewController.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/10/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit
import Foundation
import SnapKit

class SideMenuViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var titleLabel: UILabel!
    var tableView: UITableView!
    var nlButton: UIButton!
    var frButton: UIButton!
    
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    
    var cellTitleNL: [String] = ["HOME", "VERHUUR", "TE KOOP", "SERVICE & ONDERHOUD", "TRANSPORT", "OVER ONS", "NIEUWS", "NIEUWSBRIEF", "CONTACT", "VACATURES"]
    var cellTitleFR: [String] = ["HOME", "LOCATION", "A VENDRE", "SERVICE & ENTRETIEN", "TRANSPORT", "A PROPOS…", "NEWS", "NEWSLETTER",
                                 "CONTACT", "OFFERS D’EMPLOI"]
    var cellTitle: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    
    func setupViews() {
        navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = .black
        
        titleLabel = UILabel()
        titleLabel.textColor = UIColor.white
        titleLabel.backgroundColor = UIColor.black
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
        titleLabel.text = "MENU"
        
        tableView = UITableView()
        tableView.backgroundColor = UIColor.clear
        tableView.register(SideMenuTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .white
        tableView.dataSource = self
        tableView.delegate = self
        
        nlButton = UIButton()
        nlButton.setTitle("NL", for: .normal)
        nlButton.setTitleColor(.red, for: .normal)
        nlButton.tag = 0
        nlButton.addTarget(self, action: #selector(defaultLanguage(_:)), for: .touchUpInside)
        
        frButton = UIButton()
        frButton.setTitle("FR", for: .normal)
        frButton.tag = 1
        frButton.addTarget(self, action: #selector(defaultLanguage(_:)), for: .touchUpInside)
        
        self.view.addSubview(titleLabel)
        self.view.addSubview(tableView)
        self.view.addSubview(nlButton)
        self.view.addSubview(frButton)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints{ make in
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.top.equalTo(self.view).offset(10)
            make.height.equalTo(self.view).dividedBy(17)
        }
        
        tableView.snp.makeConstraints{ make in
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.bottom.equalTo(nlButton).offset(20)
        }
        
        nlButton.snp.makeConstraints{ make in
            make.height.width.equalTo(30)
            make.centerX.equalTo(tableView.snp.centerX).offset(-20)
            make.bottom.equalTo(self.view).offset(-5)
        }
        
        frButton.snp.makeConstraints{ make in
            make.height.width.equalTo(30)
            make.centerX.equalTo(tableView.snp.centerX).offset(20)
            make.bottom.equalTo(self.view).offset(-5)
        }
    }
    
    func configureViewFromLocalisation() {
        if let language = UserDefaults.standard.object(forKey: "kSaveLanguageDefaultKey") as? String {
            if language == "Netherlands_nl-NL" {
                nlButton.setTitleColor(.red, for: .normal)
                frButton.setTitleColor(.white, for: .normal)
                cellTitle = cellTitleNL
            } else {
                nlButton.setTitleColor(.white, for: .normal)
                frButton.setTitleColor(.red, for: .normal)
                cellTitle = cellTitleFR
            }
        }
        tableView.reloadData()
    }
    
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    func defaultLanguage(_ sender: UIButton) {
        if sender.tag == 0 {
            if SetLanguage(arrayLanguages[0]) {
                let alertController = UIAlertController(title: "", message: Localization("LanguageChangedText"), preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            nlButton.setTitleColor(.red, for: .normal)
            frButton.setTitleColor(.white, for: .normal)
            Localisator.sharedInstance.saveInUserDefaults = true
            configureViewFromLocalisation()
        } else{
            if SetLanguage(arrayLanguages[1]) {
                let alertController = UIAlertController(title: "", message: Localization("LanguageChangedText"), preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            nlButton.setTitleColor(.white, for: .normal)
            frButton.setTitleColor(.red, for: .normal)
            Localisator.sharedInstance.saveInUserDefaults = true
            configureViewFromLocalisation()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuTableViewCell
        cell.setupCell(title: cellTitle[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let homeVC = HomeViewController()
            self.mm_drawerController.centerViewController = UINavigationController(rootViewController: homeVC)
            homeVC.navigationController?.isNavigationBarHidden = true
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        } else if indexPath.row == 1 {
            let offersVC = OffersViewController()
            self.mm_drawerController.centerViewController = UINavigationController(rootViewController: offersVC)
            //offersVC.navigationTitle = cellTitle[indexPath.row]
            offersVC.navigationController?.isNavigationBarHidden = true
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        }else if indexPath.row == 2 {
            let forSaleVC = ForSaleViewController()
            self.mm_drawerController.centerViewController = UINavigationController(rootViewController: forSaleVC)
            forSaleVC.navigationTitle = cellTitle[indexPath.row]
            forSaleVC.navigationController?.isNavigationBarHidden = true
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        }else if indexPath.row == 3 {
            let serviceVC = ServiceViewController()
            self.mm_drawerController.centerViewController = UINavigationController(rootViewController: serviceVC)
            serviceVC.navigationTitle = cellTitle[indexPath.row]
            serviceVC.navigationController?.isNavigationBarHidden = true
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

