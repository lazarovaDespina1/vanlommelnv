//
//  VechiclesViewController.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/10/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class VehiclesViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var navigationView: NavigationView!
    var vehiclesCollectionView: UICollectionView!
    
    var vehicles: [Vehicle] = [Vehicle]()
    var navigationTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
    }

    func setupViews() {
        self.view.backgroundColor = .lightGray

        navigationView = NavigationView()
        navigationView.leftButton.setBackgroundImage(UIImage(named: "btn_back"), for: .normal)
        navigationView.leftButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        navigationView.titleLabel.text = navigationTitle
   
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: (view.frame.width/2 - 20), height:  self.view.frame.height/4)
        vehiclesCollectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        vehiclesCollectionView.backgroundColor = UIColor.clear
        vehiclesCollectionView.register(VehiclesCollectionViewCell.self, forCellWithReuseIdentifier: "vehiclesCell")
        vehiclesCollectionView.dataSource = self
        vehiclesCollectionView.delegate = self
        
        self.view.addSubview(navigationView)
        self.view.addSubview(vehiclesCollectionView)
    }
    
    func setupConstraints() {
        navigationView.snp.makeConstraints{ make in
            make.right.top.left.equalTo(self.view)
            make.height.equalTo(self.view.snp.height).dividedBy(13)
        }
        vehiclesCollectionView.snp.makeConstraints{ make in
            make.right.left.bottom.equalTo(self.view)
            make.top.equalTo(navigationView.snp.bottom)
        }
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension VehiclesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vehicles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vehiclesCell", for: indexPath) as! VehiclesCollectionViewCell
        let vechicle = vehicles[indexPath.row]
        cell.setupCell(vechicleName: vechicle.name, image: vechicle.images[0])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vehiclesDetailsVC = VechiclesDetailViewController()
        vehiclesDetailsVC.vehicle = vehicles[indexPath.row]
        self.navigationController?.pushViewController(vehiclesDetailsVC, animated: true)
    }
}

