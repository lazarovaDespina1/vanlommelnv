//
//  ServiceViewController.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/13/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class ServiceViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var navigationView: NavigationView!
    var titleLabel: UILabel!
    var descLabel: UILabel!
    var navigationTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        NotificationCenter.default.addObserver(self, selector: #selector(receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
    }
    
    func setupViews() {
        navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = .white
        
        navigationView = NavigationView()
        navigationView.leftButton.setBackgroundImage(UIImage(named: "menu"), for: .normal)
        navigationView.leftButton.addTarget(self, action: #selector(showSlideMenu), for: .touchUpInside)
        
        titleLabel = UILabel()
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
        titleLabel.numberOfLines = 0
        
        descLabel = UILabel()
        descLabel.textAlignment = .justified
        descLabel.font = UIFont.systemFont(ofSize: 20.0)
        descLabel.numberOfLines = 0
        
        configureViewFromLocalisation()
        
        self.view.addSubview(navigationView)
        self.view.addSubview(titleLabel)
        self.view.addSubview(descLabel)
    }
    
    func setupConstraints() {
        navigationView.snp.makeConstraints{ make in
            make.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.left.equalTo(self.view)
            make.height.equalTo(self.view.snp.height).dividedBy(13)
        }
        
        titleLabel.snp.makeConstraints{ make in
            make.top.equalTo(navigationView.snp.bottom).offset(20)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
        }
        
        descLabel.snp.makeConstraints{ make in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
        }
    }
    
    func showSlideMenu() {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    func configureViewFromLocalisation() {
        let title = NSMutableAttributedString(string: Localization("ServiceTitle"), attributes:  [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : UIColor.black])
        titleLabel.attributedText = title
        
        let attributedTitle =  [NSFontAttributeName : UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName : UIColor.red]
        let attributedDesc = [NSFontAttributeName : UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName : UIColor.black]
        
        let title1 = NSMutableAttributedString(string: Localization("ServiceTitle1"), attributes: attributedTitle)
        let title2 = NSMutableAttributedString(string: Localization("ServiceTitle2"), attributes: attributedTitle)
        let title3 = NSMutableAttributedString(string: Localization("ServiceTitle3"), attributes: attributedTitle)
        
        let desc1 = NSMutableAttributedString(string: Localization("ServiceText1"), attributes: attributedDesc)
        let desc2 = NSMutableAttributedString(string: Localization("ServiceText2"), attributes: attributedDesc)
        let desc3 = NSMutableAttributedString(string: Localization("ServiceText3"), attributes: attributedDesc)

        title1.append(desc1)
        title2.append(desc2)
        title3.append(desc3)
        
        title1.append(title2)
        title1.append(title3)

        descLabel.attributedText = title1
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
}


