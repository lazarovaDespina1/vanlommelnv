//
//  ForSaleViewController.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/12/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class ForSaleViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var navigationView: NavigationView!
    var titleLabel: UILabel!
    var descLabel: UILabel!
    var navigationTitle: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        NotificationCenter.default.addObserver(self, selector: #selector(receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
    }
    
    func setupViews() {
        navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = .white
        
        navigationView = NavigationView()
        navigationView.leftButton.setBackgroundImage(UIImage(named: "menu"), for: .normal)
        navigationView.leftButton.addTarget(self, action: #selector(showSlideMenu), for: .touchUpInside)
        
        titleLabel = UILabel()
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
        titleLabel.numberOfLines = 0
        
        descLabel = UILabel()
        descLabel.textAlignment = .justified
        descLabel.font = UIFont.systemFont(ofSize: 20.0)
        descLabel.numberOfLines = 0
 
        configureViewFromLocalisation()
        
        self.view.addSubview(navigationView)
        self.view.addSubview(titleLabel)
        self.view.addSubview(descLabel)
    }
    
    func setupConstraints() {
        navigationView.snp.makeConstraints{ make in
            make.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.left.equalTo(self.view)
            make.height.equalTo(self.view.snp.height).dividedBy(13)
        }
        
        titleLabel.snp.makeConstraints{ make in
            make.top.equalTo(navigationView.snp.bottom).offset(20)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
        }
       
        descLabel.snp.makeConstraints{ make in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
        }
    }
    
    func showSlideMenu() {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    func configureViewFromLocalisation() {
        titleLabel.text = Localization("ForSaleTitle")
        descLabel.text = Localization("ForSaleText")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
}

