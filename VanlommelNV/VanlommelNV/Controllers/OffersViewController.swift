//
//  OffersViewController.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/11/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class OffersViewController: UIViewController {
    
    var backImageView: UIImageView!
    var offersCollectionView : UICollectionView!
    var pageControl : UIPageControl!
    var headerCollectionView : UICollectionView!
    var headerView : HeaderView!
    var pageNumber : CGFloat!
    
    var offers: [Offer] = []
    var headerImages: [String] = []
    var headerTitles: [String] = []
    
    //MARK: Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataSource()
        setupViews()
        setupConstraints()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Setup views and controllers
    func setupViews() {
        self.view.backgroundColor = UIColor.white
        
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "background")
        
        pageControl = UIPageControl()
        pageControl.numberOfPages = headerImages.count
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.currentPage = 0
        
        offersCollectionView = UICollectionView(frame: CGRect(), collectionViewLayout: ImagesFlowLayout())
        offersCollectionView.register(OfferCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        offersCollectionView.dataSource = self
        offersCollectionView.backgroundColor = UIColor.clear
        offersCollectionView.isPagingEnabled = true
        offersCollectionView.showsHorizontalScrollIndicator = false
        offersCollectionView.setContentOffset(CGPoint(x: self.view.frame.size.width, y: 0), animated: true)
        offersCollectionView.bounces = false
        offersCollectionView.delegate = self
        
        
        headerView = HeaderView()
        headerView.delegate = self
        
        headerView.array = headerTitles
        headerView.backgroundImages = headerImages
        //  getDataSource()
        
        self.view.addSubview(backImageView)
        self.view.addSubview(headerView)
        self.view.addSubview(offersCollectionView)
    }
    
    func setupConstraints() {
        backImageView.snp.makeConstraints { make in
            make.left.right.bottom.top.equalTo(self.view)
        }
        
        headerView.snp.makeConstraints { make in
            make.left.right.top.equalTo(self.view)
            make.height.equalTo(self.view).dividedBy(3)
        }
        offersCollectionView.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(headerView.snp.bottom).offset(-20)
        }
    }
    
    func getDataSource() {
        var url = Bundle.main.url(forResource: "vechicles-FR", withExtension: "json")
        if let language = UserDefaults.standard.object(forKey: "kSaveLanguageDefaultKey") as? String {
            if language == "Netherlands_nl-NL" {
                url = Bundle.main.url(forResource: "vechices-NL", withExtension: "json")
            }
        }
        
        let data = try! Data(contentsOf: url!)
        let JSON = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
        let allOffers = JSON["offers"] as! [[String:Any]]
        
        for offer in allOffers {
            offers.append(Offer(offer: offer))
            headerImages.append(offer["offerImageName"] as! String)
            headerTitles.append(offer["offerName"] as! String)
        }
        //        headerView.array = headerTitles
        //        headerView.backgroundImages = headerImages
        // offersCollectionView.reloadData()
    }
    
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            getDataSource()
        }
    }
    
    func showSlideMenu() {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    
    
    func animateHeaderCollectionView(fromScroll : Bool) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0.2, options: UIViewAnimationOptions.curveLinear, animations: {
                if self.pageNumber != 0{
                    self.headerView.collectionView.contentOffset.x = CGFloat(Int(self.pageNumber) - 1) * self.view.frame.size.width/3
                } else {
                    self.headerView.collectionView.contentOffset.x = -self.view.frame.size.width/3
                }
                
                if !fromScroll {
                    self.offersCollectionView.contentOffset.x = self.pageNumber * self.view.frame.size.width
                }
                self.headerView.pageWhereLineisShown = Int(self.pageNumber)
                self.headerView.collectionView.reloadData()
            }, completion: nil)
            self.view.layoutIfNeeded()
        }
    }
}

extension OffersViewController: OfferCellDelegate {

    func presentVC(vehicle: Vehicle) {
        let vehicleVC = VechiclesDetailViewController()
        vehicleVC.vehicle = vehicle
        navigationController?.pushViewController(vehicleVC, animated: true)
    }
}

//MARK: CollectionView Delegate and DataSource
extension OffersViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return headerImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OfferCollectionViewCell
        //let vehicles = offers[indexPath.row].vehicles
        cell.vehicles = offers[indexPath.row].vehicles
        cell.offerDelegate = self
        cell.tableView.reloadData()
        return cell
    }
}

//MARK: ScrollView Delegate
extension OffersViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == headerCollectionView  {
            
            pageNumber = round(offersCollectionView.contentOffset.x / offersCollectionView.frame.size.width)
            // pageControl.currentPage = Int(pageNumber)
            
            if (scrollView == headerCollectionView) {
                offersCollectionView.contentOffset.x = scrollView.contentOffset.x
            }
        }
        pageNumber = round(offersCollectionView.contentOffset.x / offersCollectionView.frame.size.width)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //        if scrollView == hotelImagesCollectionView {
        //            timer.invalidate()
        //        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (scrollView == offersCollectionView) {
            animateHeaderCollectionView(fromScroll: true)
        }
    }
}

extension OffersViewController : HeaderViewDelegate {
    func collectionViewDidScroll(contentX: CGFloat) {
        // hotelImagesCollectionView.contentOffset.x = contentX
    }
    
    func didSelectCollectionViewCell(indexRow: Int) {
        pageNumber = CGFloat(indexRow)
        animateHeaderCollectionView(fromScroll: false)
    }
}
