import UIKit
import MMDrawerController
import YouTubePlayer
import youtube_ios_player_helper

class HomeViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var navigationView: NavigationView!
    var backImageView: UIImageView!
    var scrollView: UIScrollView!
    var descLabel: UILabel!
    var webView: UIWebView!
    var separatorLabel1: UILabel!
    var separatorLabel2: UILabel!
    var infoLabel: UILabel!
    var infoBoxLabel: UILabel!
    var contactButton: UIButton!
    var subscribeTitleLabel: UILabel!
    var subsribeBoxView: UIView!
    var lastNameTextField: UITextField!
    var firstNameTextField: UITextField!
    var emailTextField: UITextField!
    var subscribeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        NotificationCenter.default.addObserver(self, selector: #selector(receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
    }
    
    func setupViews() {
        navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = .white
        
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "background")
        
        navigationView = NavigationView()
        navigationView.leftButton.setBackgroundImage(UIImage(named: "menu"), for: .normal)
        navigationView.leftButton.addTarget(self, action: #selector(showSlideMenu), for: .touchUpInside)
        
        scrollView = UIScrollView()
        scrollView.delegate = self
        scrollView.contentInsetAdjustmentBehavior = .automatic
        //scrollView.contentSize = CGSize(width: 0, height: 1000)
        
        descLabel = UILabel()
        descLabel.textAlignment = .left
        descLabel.numberOfLines = 0
        descLabel.layer.masksToBounds = true
        descLabel.layer.cornerRadius = 10.0
        descLabel.backgroundColor = .clear
    //    descLabel.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        
        webView = UIWebView()
        let myVideoURL = URL(string: "https://www.youtube.com/embed//uNDUZbwtruM")
        webView.loadRequest(URLRequest(url: myVideoURL!))
        webView.backgroundColor = .clear
        
        separatorLabel1 = separatorLabel()
        separatorLabel2 = separatorLabel()
        infoLabel = titleLabel()
        
        infoBoxLabel = UILabel()
        infoBoxLabel.textAlignment = .left
        infoBoxLabel.numberOfLines = 0
        infoBoxLabel.backgroundColor = UIColor.clear
        
        contactButton = UIButton()
        contactButton.backgroundColor = UIColor.red
        contactButton.setTitleColor(.white, for: .normal)
        
        subsribeBoxView = UIView()
        subsribeBoxView.backgroundColor = UIColor.clear
        
        subscribeTitleLabel = titleLabel()
        
        lastNameTextField = UITextField()
        lastNameTextField.returnKeyType = .next
        lastNameTextField.textColor = UIColor.black
        lastNameTextField.backgroundColor = UIColor.white
        // lastNameTextField.delegate = self
        // lastNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)]
        
        firstNameTextField = UITextField()
        firstNameTextField.returnKeyType = .next
        firstNameTextField.textColor = UIColor.black
        firstNameTextField.backgroundColor = UIColor.white
        
        emailTextField = UITextField()
        emailTextField.returnKeyType = .done
        emailTextField.textColor = UIColor.black
        emailTextField.backgroundColor = UIColor.white
        
        subscribeButton = UIButton()
        subscribeButton.backgroundColor = UIColor.red
        subscribeButton.setTitleColor(.white, for: .normal)
        
        configureViewFromLocalisation()
        
        self.view.addSubview(backImageView)
        self.view.addSubview(navigationView)
        self.view.addSubview(scrollView)
        scrollView.addSubview(descLabel)
        scrollView.addSubview(webView)
        scrollView.addSubview(separatorLabel1)
        scrollView.addSubview(infoLabel)
        scrollView.addSubview(infoBoxLabel)
        infoBoxLabel.addSubview(contactButton)
        scrollView.addSubview(separatorLabel2)
        scrollView.addSubview(subscribeTitleLabel)
        scrollView.addSubview(subsribeBoxView)
        subsribeBoxView.addSubview(lastNameTextField)
        subsribeBoxView.addSubview(firstNameTextField)
        subsribeBoxView.addSubview(emailTextField)
        subsribeBoxView.addSubview(subscribeButton)
    }
    
    func setupConstraints() {
        backImageView.snp.makeConstraints{ make in
            make.edges.equalTo(self.view)
        }
        
        navigationView.snp.makeConstraints{ make in
            make.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.left.equalTo(self.view)
            make.height.equalTo(self.view.snp.height).dividedBy(13)
        }
        
        scrollView.snp.makeConstraints{ make in
            make.top.equalTo(navigationView.snp.bottom)
            make.left.equalTo(self.view)
            make.bottom.right.equalTo(self.view)
        }
        
        descLabel.snp.makeConstraints{ make in
            make.top.equalTo(scrollView).offset(20)
            make.left.equalTo(scrollView).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.bottom.equalTo(webView.snp.top).offset(-20)
        }
        
        webView.snp.makeConstraints{ make in
            make.top.equalTo(descLabel.snp.bottom).offset(20)
            make.left.equalTo(scrollView).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.height.equalTo(self.view.frame.height/3)
        }
        
        separatorLabel1.snp.makeConstraints{ make in
            make.top.equalTo(webView.snp.bottom).offset(20)
            make.left.equalTo(scrollView).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.height.equalTo(5)
        }
        
        infoLabel.snp.makeConstraints{ make in
            make.top.equalTo(separatorLabel1.snp.bottom).offset(20)
            make.left.equalTo(scrollView).offset(10)
            make.right.equalTo(self.view).offset(-10)
        }
        
        infoBoxLabel.snp.makeConstraints{ make in
            make.top.equalTo(infoLabel.snp.bottom).offset(10)
            make.left.equalTo(scrollView).offset(10)
            make.right.equalTo(self.view).offset(-10)
            //make.bottom.equalTo(scrollView.snp.bottom).offset(-20)
        }
        
        contactButton.snp.makeConstraints{ make in
            make.width.equalTo(self.view.frame.width/2)
            make.height.equalTo(35)
            make.right.equalTo(infoBoxLabel.snp.right).offset(-10)
            make.bottom.equalTo(infoBoxLabel.snp.bottom).offset(-20)
        }
        
        separatorLabel2.snp.makeConstraints{ make in
            make.top.equalTo(infoBoxLabel.snp.bottom).offset(20)
            make.left.equalTo(scrollView).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.height.equalTo(5)
            // make.bottom.equalTo(scrollView.snp.bottom).offset(-20)
        }
        
        subscribeTitleLabel.snp.makeConstraints{ make in
            make.top.equalTo(separatorLabel2.snp.bottom).offset(10)
            make.left.equalTo(scrollView).offset(10)
            make.right.equalTo(self.view).offset(-10)
            //make.bottom.equalTo(scrollView.snp.bottom).offset(-20)
        }
        
        subsribeBoxView.snp.makeConstraints{ make in
            make.top.equalTo(subscribeTitleLabel.snp.bottom).offset(10)
            make.left.equalTo(scrollView).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.height.equalTo(300)
            make.bottom.equalTo(scrollView.snp.bottom).offset(-20)
        }
        
        lastNameTextField.snp.makeConstraints{ make in
            make.top.equalTo(subsribeBoxView.snp.top).offset(30)
            make.left.equalTo(subsribeBoxView.snp.left).offset(20)
            make.right.equalTo(subsribeBoxView.snp.right).offset(-20)
            make.height.equalTo(40)
        }
        
        firstNameTextField.snp.makeConstraints{ make in
            make.top.equalTo(lastNameTextField.snp.bottom).offset(10)
            make.left.equalTo(subsribeBoxView.snp.left).offset(20)
            make.right.equalTo(subsribeBoxView.snp.right).offset(-20)
            make.height.equalTo(40)
        }
        
        emailTextField.snp.makeConstraints{ make in
            make.top.equalTo(firstNameTextField.snp.bottom).offset(10)
            make.left.equalTo(subsribeBoxView.snp.left).offset(20)
            make.right.equalTo(subsribeBoxView.snp.right).offset(-20)
            make.height.equalTo(40)
        }
        
        subscribeButton.snp.makeConstraints{ make in
            make.width.equalTo(self.view.frame.width/2)
            make.height.equalTo(35)
            make.right.equalTo(subsribeBoxView.snp.right).offset(-10)
            make.bottom.equalTo(subsribeBoxView.snp.bottom).offset(-20)
        }
    }
    
    
    func separatorLabel() -> UILabel {
        let label = UILabel()
        label.backgroundColor = UIColor.lightGray
        return label
    }
    
    func titleLabel() -> UILabel {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 25.0)
        label.backgroundColor = UIColor.clear
        
        return label
    }
    
    func showSlideMenu() {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    func configureViewFromLocalisation() {
        let attributedTitle = NSMutableAttributedString(string: Localization("HomeTitleText"), attributes:  [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : UIColor.white])
        let attributedMainDesc = NSMutableAttributedString(string: Localization("HomeMainDescText"), attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName : UIColor.red])
        let attributedDesc = NSMutableAttributedString(string: Localization("HomeDescText"), attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName : UIColor.white])
        attributedTitle.append(attributedMainDesc)
        attributedTitle.append(attributedDesc)
        descLabel.attributedText = attributedTitle
        
        infoLabel.text = Localization("HomeInfoText")
        
        let myString = Localization("HomeInfoBoxText")
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName : UIColor.white])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: NSRange(location:0,length:15))
        infoBoxLabel.attributedText = myMutableString
        contactButton.setTitle(Localization("HomeContactButtonTitle"), for: .normal)
        
        subscribeTitleLabel.text = Localization("HomeSubscribeTitle")
        lastNameTextField.placeholder = Localization("HomeLastNameText")
        firstNameTextField.placeholder = Localization("HomeFirstNameText")
        emailTextField.placeholder = "Email"
        subscribeButton.setTitle(Localization("HomeSubscribeButtonTitle"), for: .normal)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}
