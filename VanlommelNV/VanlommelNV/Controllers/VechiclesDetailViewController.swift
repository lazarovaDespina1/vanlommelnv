//
//  VechiclesDetailViewController.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/11/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class VechiclesDetailViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var navigationView: NavigationView!
    var scrollView: UIScrollView!    
    var tableView: UITableView!
    var pageControl: UIPageControl!
    
    var timer: Timer! = nil
    var vehicle: Vehicle?
    var navigationTitle: String = ""
    var pageImages:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.layoutSubviews()
    }
    
    func setupViews() {
        self.view.backgroundColor = .white
        
        navigationView = NavigationView()
        navigationView.leftButton.setBackgroundImage(UIImage(named: "btn_back"), for: .normal)
        navigationView.leftButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        navigationView.titleLabel.text = vehicle?.name
        
        pageImages = (vehicle?.images)!
        
        pageControl = UIPageControl(frame: CGRect(x:0,y: 0, width:200, height:50))
        pageControl.numberOfPages = pageImages.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.red
        pageControl.pageIndicatorTintColor = UIColor.black
        pageControl.currentPageIndicatorTintColor = UIColor.gray
        
        scrollView = UIScrollView(frame: CGRect(x:0, y:0, width:self.view.frame.size.width,height: self.view.frame.size.height/3))
        var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width:self.scrollView.frame.size.width * 3, height: scrollView.frame.size.height)
  
        for index in 0..<pageImages.count {
            frame.origin.x = CGFloat(index) * scrollView.frame.size.width
            frame.size = CGSize(width:self.scrollView.frame.size.width, height: scrollView.frame.size.height)
            
           // frame.size = self.scrollView.frame.size

            let img = UIImageView(frame: frame)
            img.contentMode = .scaleAspectFit
            img.kf.setImage(with: URL(string: pageImages[index]))

            self.scrollView.addSubview(img)
        }
        
        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        scrollView.backgroundColor = .white
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        scrollView.addGestureRecognizer(singleTap)
        scrollView?.showsHorizontalScrollIndicator = false
        
        tableView = UITableView()
        tableView.backgroundColor = UIColor.white
        tableView.register(SideMenuTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.view.addSubview(navigationView)
        self.view.addSubview(scrollView)
        self.view.addSubview(pageControl)
        self.view.addSubview(tableView)
    }
    
    func setupConstraints() {
        navigationView.snp.makeConstraints{ make in
            make.right.top.left.equalTo(self.view)
            make.height.equalTo(self.view.snp.height).dividedBy(13)
        }
        scrollView.snp.makeConstraints{ make in
            make.top.equalTo(navigationView.snp.bottom).offset(20)
            make.right.left.equalTo(self.view)
            make.height.equalTo(self.view.frame.size.height/3)
        }
        pageControl.snp.makeConstraints{ make in
            make.top.equalTo(scrollView.snp.bottom).offset(10)
            make.centerX.equalTo(scrollView.snp.centerX)
        }
        tableView.snp.makeConstraints{ make in
            make.top.equalTo(pageControl.snp.bottom).offset(20)
            make.right.left.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func handleTap(_ recognizer: UITapGestureRecognizer) {
        let screenWidth = UIScreen.main.bounds.width
        let recognizedTapLocation = recognizer.location(in: scrollView).x
        if 0...screenWidth ~= recognizedTapLocation{
            print("First")
        } else if screenWidth...(screenWidth*2) ~= recognizedTapLocation {
        } else if (screenWidth*2)...(screenWidth*3) ~= recognizedTapLocation {
            print("third")
        }
    }
    
    func moveToNextPage() {
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 3
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        var pageNumber: CGFloat
        var slideToX = contentOffset + pageWidth
        
        pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
        if  contentOffset + pageWidth == maxWidth {
            slideToX = 0
            pageNumber = 0
        }
        pageControl.currentPage = Int(pageNumber)
        self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension VechiclesDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0.0
        }
        
    }
}

extension VechiclesDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return (vehicle?.details.count)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (vehicle?.details[section].properties.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuTableViewCell
        
        let properties = vehicle?.details[indexPath.section].properties
        cell.setupCell(title:" - " + properties![indexPath.row])
        cell.contentView.backgroundColor = .white
        cell.titleLabel.textColor = .black
        cell.titleLabel.textAlignment = .left
        cell.titleLabel.font = UIFont.systemFont(ofSize: 18.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44))
        view.backgroundColor = .white
        let label = UILabel()
        label.text = vehicle?.details[section].detailTitle
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        view.addSubview(label)
        label.snp.makeConstraints{ make in
            make.left.equalTo(view).offset(10)
            make.top.bottom.right.equalTo(view)
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = UIColor.clear
        return vw
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

