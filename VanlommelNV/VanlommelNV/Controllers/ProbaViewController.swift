//
//  ProbaViewController.swift
//  VanlommelNV
//
//  Created by Miki Dimitrov on 4/13/18.
//  Copyright © 2018 ECHOiOS. All rights reserved.
//

import UIKit

class ProbaViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var navImageView: UIImageView!
    var navData = ["gradient", "logo"]
    
    var offersCollectionView: UICollectionView!
    var titleLabel: UILabel!
    var offers: [Offer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        NotificationCenter.default.addObserver(self, selector: #selector(receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
    }
    
    func setupViews() {
        self.view.backgroundColor = .lightGray
        
        navImageView = UIImageView()
        navImageView.image = UIImage(named: navData[0])
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: (view.frame.width/2 - 20), height:  self.view.frame.height/4)
        offersCollectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        offersCollectionView.backgroundColor = UIColor.clear
        offersCollectionView.register(VehiclesCollectionViewCell.self, forCellWithReuseIdentifier: "vehiclesCell")
        offersCollectionView.dataSource = self
        offersCollectionView.delegate = self
        
        getDataSource()
        
        self.view.addSubview(offersCollectionView)
    }
    
    func setupConstraints() {
       
        offersCollectionView.snp.makeConstraints{ make in
            make.right.left.bottom.equalTo(self.view)
           // make.top.equalTo(titleLabel.snp.bottom)
        }
    }
    
    func getDataSource() {
        var url = Bundle.main.url(forResource: "vechicles-FR", withExtension: "json")
        if let language = UserDefaults.standard.object(forKey: "kSaveLanguageDefaultKey") as? String {
            if language == "Netherlands_nl-NL" {
                url = Bundle.main.url(forResource: "vechices-NL", withExtension: "json")
            }
        }
        
        let data = try! Data(contentsOf: url!)
        let JSON = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
        let allOffers = JSON["offers"] as! [[String:Any]]
        
        for offer in allOffers {
            offers.append(Offer(offer: offer))
        }
        
       // titleLabel.text = JSON["title"] as? String
        offersCollectionView.reloadData()
    }
    
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            getDataSource()
        }
    }
    
    func showSlideMenu() {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
}

extension ProbaViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vehiclesCell", for: indexPath) as! VehiclesCollectionViewCell
        let offer = offers[indexPath.row]
        cell.setupCell(vechicleName: offer.offerName, image: offer.offerImage)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vehiclesVC = VehiclesViewController()
        let offer = offers[indexPath.row]
        vehiclesVC.navigationTitle = offer.offerName
        vehiclesVC.vehicles = offer.vehicles
        
        self.navigationController?.pushViewController(vehiclesVC, animated: true)
    }
}

